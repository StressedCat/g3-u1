#include <iostream>
#include <algorithm>
using namespace std;

#include "main.h"
#include "lista.h"

lista::lista(){}

/* por medio de asignar un nuevo nodo, se realiza la operacion de crear un
 * nuevo elemento para la lista enlazada, donde el null se moverá un espacio
 * asi designar un nuevo espacio, el caso del null es para saber cuando la
 * lista enlazada va a terminar.
 */
void lista::make(info *informacion){
  Node *tmp;

  tmp = new Node;
  tmp->informacion = informacion;
  tmp->sig = NULL;

  if(this->raiz == NULL){
    this->raiz = tmp;
    this->ultimo = this->raiz;
  }
  else{
    this->ultimo->sig = tmp;
    this->ultimo = tmp;
  }
}

/* Aca se mostrará la lista enlazada que se ingreso, para mostrarlos en orden
 * se usa una biblioteca llamada algorithm, la cual realiza la operacion de
 * realizar sort en una lista de una dimension sobre los numeros que fueron
 * ingresados en la lista enlazada, las dos listas ingresarán a este proceso
 * en donde los numeros de las dos se guardarán en la misma lista de una
 * dimensión
 */
void lista::showsorted(int ini, int fin, int *list){
  int listanum[fin];
  int x = 0;
  Node *tmp = this->raiz;
  cout << "Estos fueron sus numeros ingresados: " << endl;
  while(tmp != NULL){
    listanum[x] = tmp->informacion->get_num();
    list[ini+x] = tmp->informacion->get_num();
    tmp = tmp->sig;
    x = x + 1;
  }
  sort(listanum, listanum+fin);
  for (int i = 0; i < fin; i++) {
    cout << "numero: " << listanum[i] << endl;
  }
}
