#include <iostream>
#include <algorithm>
using namespace std;

#include "main.h"
#include "info.h"
#include "lista.h"

/* Se realiza la operacion para crear una lista enlazada, donde se asignara
 * al inicio un NULL, en el caso que este vacia, asi no queda en un ciclo
 * infinito
 */
class Start{
  private:
    lista *list = NULL;

  public:
    Start(){
      this->list = new lista();
    }

    lista *get_lista(){
      return this->list;
    }
};


int main(){
  Start l1 = Start();
  lista *list1 = l1.get_lista();
  Start l2 = Start();
  lista *list2 = l2.get_lista();
  Start l3 = Start();
  lista *listmix = l3.get_lista();
  bool flick = true;
  int num;
  int x = 0;
  int y = 0;
  int retry;

  /*
   * se crea la primera lista, donde el usuario ingresa los numeros que desee
   */
  while (flick != false) {
    cout << "Ingrese un numero a la lista 1" << endl;
    cin >> num;
    if (x == 0) {
      info *inicial = new info(num);
      list1->make(inicial);
    }
    else{
      list1->make(new info(num));
    }
    cout << "Si desea dejar de agregar numeros, ingresar 0" << endl;
    cin >> retry;
    x = x + 1;
    if (retry == 0){
      flick = false;
    }
  }
  /* se reinicia el flick para la segunda lista */
  flick = true;
  /*
   * se crea la segunda lista, donde el usuario ingresa los numeros que desee
   */
  while (flick != false) {
    cout << "Ingrese un numero a la lista 2" << endl;
    cin >> num;
    if (y == 0) {
      info *inicial = new info(num);
      list2->make(inicial);
    }
    else{
      list2->make(new info(num));
    }
    cout << "Si desea dejar de agregar numeros, ingresar 0" << endl;
    cin >> retry;
    y = y + 1;
    if (retry == 0){
      flick = false;
    }
  }
  /*
   * Se crea una lista de una dimension el cual tendrá el tamaño de ambas
   * listas ingresadas anteriormente por el usuario, en donde entrará a las
   * funciones showsorted para mostrar que numeros son y ingresar los numeros
   * a esta lista
   */
  int listaint[x+y];
  cout << "Lista 1: " << endl;
  list1->showsorted(0, x, listaint);
  cout << "Lista 2: " << endl;
  list2->showsorted(x, y, listaint);
  /*
   * Cuando estos dos listas son mostradas, se crea la tercera lista gracias a
   * la ayuda de la lista que se fue creando mientras se muestran los numeros
   * que el usuario ingreso, pasando por el mismo proceso que las otras dos y
   * mostrando la lista final
   */
  for (int i = 0; i < (x+y); i++) {
    if (x == 0) {
      info *inicial = new info(listaint[i]);
      listmix->make(inicial);
    }
    else{
      listmix->make(new info(listaint[i]));
    }
  }
  cout << "Lista 1 y 2: " << endl;
  listmix->showsorted(0, x+y, listaint);
}
