Para iniciar el programa:

1 Ingrese en el CMD el comando make para instalar el programa
  -> .../g3-u1/ej2$ make

2 Se creará un programa con nombre de Cargo, para ingresar a este
  haga lo siguiente:
  -> .../g3-u1/ej2$ ./DosListasInt


En el programa:

  El programa consiste en realizar dos listas enlazadas, la cual el usuario
  ingresará libremente en excepcion al 0.

  Primero el usuario crea la lista numero uno, la cual puede tener el tamaño
  que se desea y es flexible en ese sentido, porque el tamaño no es
  predeterminado.

  Se realizará lo mismo para la segunda lista

  Al realizar ambas listas, estas se van a mezclar en una lista enlazada, de
  menor a mayor


Para borrar el programa:

  Asegurese que esté en la misma carpeta donde corrio el programa
  -> .../g3-u1/ej2

  Ingrese el comando make clean para borrar el programa que uso
  -> .../g3-u1/ej2$ make clean
