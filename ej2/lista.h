#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class lista{
  private:
    Node *raiz = NULL;
    Node *ultimo = NULL;

  public:
    lista();

    void make(info *informacion);
    void showsorted(int ini, int fin, int *list);
};
#endif
