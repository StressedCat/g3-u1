#include <iostream>
#include <algorithm>
using namespace std;

#include "main.h"
#include "lista.h"

lista::lista(){}

/* por medio de asignar un nuevo nodo, se realiza la operacion de crear un
 * nuevo elemento para la lista enlazada, donde el null se moverá un espacio
 * asi designar un nuevo espacio, el caso del null es para saber cuando la
 * lista enlazada va a terminar.
 */
void lista::make(info *informacion){
  Node *tmp;

  tmp = new Node;
  tmp->informacion = informacion;
  tmp->sig = NULL;

  if(this->raiz == NULL){
    this->raiz = tmp;
    this->ultimo = this->raiz;
  }
  else{
    this->ultimo->sig = tmp;
    this->ultimo = tmp;
  }
}

/*
 * Esta funcion es para mostrar la lista final, donde usando la lista enlazada
 * creada anteriormente se obtienen los resultados de esta
 */
void lista::showlista() {
  Node *tmp = this->raiz;
  cout << "Esta es su lista rellenada: " << endl;
  while(tmp != NULL){
    cout << "[" << tmp->informacion->get_num() << "] ";
    tmp = tmp->sig;
  }
  cout << endl;
}

void lista::showsorted(int *list){
  int x = 0;
  Node *tmp = this->raiz;
  cout << "Esta es su lista: " << endl;
  while(tmp != NULL){
    list[x] = tmp->informacion->get_num();
    /* se mueve al siguiente elemento de la lista */
    tmp = tmp->sig;
    /* x es para ubicar la posicion de la lista, cada vez que se repite sube
     * uno mas
     */
    x = x + 1;
  }
  /* se realiza la operacion sort, donde en el segundo se decide hasta que
   * numero se deberia ordenar
   */
  sort(list, list+x);
  /* Se mostrará la lista ordenada para hacerse una idea de que numeros
   * tendrán que ser rellenados
   */
  for (int i = 0; i < x; i++) {
    cout << "[" << list[i] << "] ";
  }
  cout << endl;
}
