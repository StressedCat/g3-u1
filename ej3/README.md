Para iniciar el programa:

1 Ingrese en el CMD el comando make para instalar el programa
  -> .../g3-u1/ej3$ make

2 Se creará un programa con nombre de Cargo, para ingresar a este
  haga lo siguiente:
  -> .../g3-u1/ej3$ ./Rellenar


En el programa:

  Se le pide al usuario que ingrese numeros a una lista enlazada

  Esta lista creada, se usará para despues rellenar los numeros que estan entre
  el menor y el mayor, creando una segunda lista enlazada que tiene todo los
  numeros faltantes


Para borrar el programa:

  Asegurese que esté en la misma carpeta donde corrio el programa
  -> .../g3-u1/ej3

  Ingrese el comando make clean para borrar el programa que uso
  -> .../g3-u1/ej3$ make clean
