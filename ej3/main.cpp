#include <iostream>
#include <algorithm>
using namespace std;

#include "main.h"
#include "info.h"
#include "lista.h"

/* Se realiza la operacion para crear una lista enlazada, donde se asignara
 * al inicio un NULL, en el caso que este vacia, asi no queda en un ciclo
 * infinito
 */
class Start{
  private:
    lista *list = NULL;

  public:
    Start(){
      this->list = new lista();
    }

    lista *get_lista(){
      return this->list;
    }
};

/*
 * Para rellenar, los numeros mas importantes son el mayor y el menor, los
 * otros numeros son ignorados por completo, creando una lista de una dimension
 * que obtendrá todo los numeros para rellenar dicha lista
 */
void rellenar(int ini, int fin, int *list) {
  int x = 0;
  for (int i = ini; i < fin+1; i++){
    list[x] = i;
    x = x + 1;
  }
}


int main(){
  Start l1 = Start();
  lista *list1 = l1.get_lista();
  Start l2 = Start();
  lista *listaFill = l2.get_lista();
  bool flick = true;
  int num;
  int x = 0;
  int y = 0;
  int retry;

  /*
   * se crea una lista, donde el usuario ingresa los numeros que desee
   */
  while (flick != false) {
    cout << "Ingrese un numero a la lista 1" << endl;
    cin >> num;
    if (x == 0) {
      info *inicial = new info(num);
      list1->make(inicial);
    }
    else{
      list1->make(new info(num));
    }
    cout << "Si desea dejar de agregar numeros, ingresar 0" << endl;
    cin >> retry;
    x = x + 1;
    if (retry == 0){
      flick = false;
    }
  }
  /* se crea una lista de una dimension para mostrar la lista ingresada por
   * el usuario, de una forma ordenada de mayor a menor
   */
  int listaini[x];
  list1->showsorted(listaini);

  /*
   * por medio de la lista creada anteriormente la cual esta ordenada de mayor a
   * menor, se obtendrá cual es el numero mayor y cual es el menor y ademas
   * el tamaño que debe tener la lista por medio de obtener el rango del menor
   * y el mayor anteriormente dichos
   */
  int ini = listaini[0];
  int fin = listaini[x-1];
  int tam = (fin - ini);
  int listarellenada[tam];
  /* se crea la lista rellenada */
  rellenar(ini, fin, listarellenada);
  /* la lista rellenada es transladada a una lista enlazada */
  for(int i = 0; i < tam+1; i++){
    if (i == 0) {
      info *inicial = new info(listarellenada[i]);
      listaFill->make(inicial);
    }
    else{
      listaFill->make(new info(listarellenada[i]));
    }
  }
  /* Se muestra la lista rellenada  */
  listaFill->showlista();
}
