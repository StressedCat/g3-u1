#include <iostream>
using namespace std;

#ifndef INFO_H
#define INFO_H

class info{
  private:
    int num;

  public:
    info(int num);

    int get_num();
};
#endif
