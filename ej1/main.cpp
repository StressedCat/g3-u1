#include <iostream>
using namespace std;

#include "main.h"
#include "info.h"
#include "lista.h"

/* Se realiza la operacion para crear una lista enlazada, donde se asignara
 * al inicio un NULL, en el caso que este vacia, asi no queda en un ciclo
 * infinito
 */
class Start{
  private:
    lista *list = NULL;

  public:
    Start(){
      this->list = new lista();
    }

    lista *get_lista(){
      return this->list;
    }
};

int main(){
  Start l1 = Start();
  lista *listInt = l1.get_lista();
  bool flick = true;
  int num;
  int x = 0;
  int retry;

  while (flick != false) {
    cout << "Ingrese un numero" << endl;
    cin >> num;
    if (x == 0) {
      info *inicial = new info(num);
      listInt->make(inicial);
    }
    else{
      listInt->make(new info(num));
    }
    cout << "Si desea dejar de agregar numeros, ingresar 0" << endl;
    cin >> retry;
    if (retry != 0) {
      x = x + 1;
    }
    else{
      flick = false;
    }
  }
  listInt->show(x);
}
