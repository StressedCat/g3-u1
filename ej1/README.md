Para iniciar el programa:


1 Ingrese en el CMD el comando make para instalar el programa
  -> .../g3-u1/ej1$ make


2 Se creará un programa con nombre de Cargo, para ingresar a este
  haga lo siguiente:
  -> .../g3-u1/ej1$ ./ListasInt


En el programa:

  El programa consiste en realizar una lista enlazada, donde el usuario ingresa
  los numeros que desea excepto el 0, ya que la funcion del 0 es para dejar de
  ingresar numeros, el programa luego realiza la operacion de mostrar estos
  numeros en un orden de menor a mayor


Para borrar el programa:

  Asegurese que esté en la misma carpeta donde corrio el programa
  -> .../g3-u1/ej1

  Ingrese el comando make clean para borrar el programa que uso
  -> .../g3-u1/ej1$ make clean
